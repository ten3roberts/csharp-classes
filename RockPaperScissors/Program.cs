﻿using System;
using System.Data;
using System.Transactions;

namespace Uppgift5
{
    class Program
    {
        static string Prompt(string prompt)
        {
            Console.Write(prompt);
            return Console.ReadLine();
        }

        static bool PromptYes(string prompt)
        {
            Console.Write(prompt);
            switch (Console.ReadLine().ToLower().Trim())
            {
                case "yes":
                    return true;
                case "y": return true;
                case "yup": return true;
                case "ok": return true;
                case "sure": return true;
                default: return false;
            }
        }

        static readonly string[] noReplies = { "You don't really have a choice here", "Did you think that was a question more than a statement?", "How fun can you be?", "I know you really want to" };
        static readonly string[] yesReplies = {  "Great, lets go", "Its'a me, fun times", "Ok, hope you're not a bad loser"};
        static void Main(string[] args)
        {
            bool choice = PromptYes("Do you want to play Rock Paper Scissors? ");
            Random random = new Random();
            // Program will always win if set to true
            // Will be set when program replies "Ok, hope you're not a bad loser"
            bool alwaysWin = false;
            if (choice)
            {
                int replyIdx = random.Next(0, yesReplies.Length);
                Console.WriteLine(yesReplies[replyIdx]);
                // Hope you're not a bad loser case to always win
                if (replyIdx == 2) alwaysWin = true;
            }
            else
            {
                Console.WriteLine(noReplies[random.Next(0, noReplies.Length)]);
            }


            string input = Prompt("You go first\nUse (r)ock, (p)apers, or (s)cissors > ");
            Attack attack = StringToAttack(input);
            if (attack == Attack.Invalid)
            {
                Console.WriteLine("Please enter a valid attack");
                return;
            }

            Attack counter = Attack.Invalid;
            if(alwaysWin)
            {
                counter = GetCounter(attack);
            }
            else
            {
                counter = (Attack)random.Next(0, 3);
            }

            Console.WriteLine($"I use {counter}");

            // Decide win
            if (attack == counter)
            {
                Console.WriteLine("It's a draw");
            }

            if(attack == GetCounter(counter))
            {
                Console.WriteLine("You win");
            }
            else if (counter == GetCounter(attack))
            {
                Console.WriteLine("I win");
            }
            else
            {
                Console.WriteLine("No one wins");
            }
        }

        static Attack StringToAttack(string str)
        {
            switch (str.ToLower())
            {
                case "rock": return Attack.Rock;
                case "r": return Attack.Rock; 
                case "paper": return Attack.Paper; 
                case "p": return Attack.Paper; 
                case "s": return Attack.Scissors;
                case "scissor": return Attack.Scissors;
                default: return Attack.Invalid;
            }
            
        }

        static Attack GetCounter(Attack attack)
        {
            switch (attack)
            {
                case Attack.Invalid: return Attack.Invalid;
                case Attack.Paper: return Attack.Scissors;
                case Attack.Rock: return Attack.Paper;
                case Attack.Scissors: return Attack.Rock;
                default: return Attack.Invalid;  // Shouldn't be needed
            }
        }

        enum Attack
        {
            Invalid = -1, Rock=0,Paper=1,Scissors=2
        }
    }
}
