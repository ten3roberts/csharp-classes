﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Runtime.InteropServices;

class Program
{

    static void PrintArray<T>(T[] array)
    {
        for (int i = 0; i < array.Length; i++)
        {
            Console.Write(array[i]);
            if (i < array.Length - 1) Console.Write(", ");
        }
        Console.WriteLine("");
    }

    static int[] ReadArray()
    {
        int length = ReadInt("Input number of elements in array: ");

        return ReadArrayWithLength(length);
    }

    static int[] ReadArrayWithLength(int length)
    {
        int[] array = new int[length];

        Console.WriteLine($"Input {length} elements into array: ");
        for (int i = 0; i < length; i++)
        {
            int num = ReadInt("");
            array[i] = num;
        }
        return array;
    }

    static int ReadInt(string prompt)
    {
        Console.Write(prompt);
        while(true)
        {
            string line = Console.ReadLine();
            int result = 0;
            if (Int32.TryParse(line, out result)) return result;
            Console.WriteLine("Invalid input");
        }
    }

    static void Task1()
    {
        Console.WriteLine("Task 1");

        Console.WriteLine("Input 10 elements into array: ");
        int[] array = new int[10];
        for (int i = 0; i < array.Length; i++)
        {
            int num = ReadInt("");
            array[i] = num;
        }

        Console.Write("Element in array are: ");

        PrintArray(array);
    }

    static void Task2()
    {
        Console.WriteLine("Task 2");

        int[] array = ReadArray();
        PrintArray(array);
    }

    static void Task3()
    {
        Console.WriteLine("Task 3");

        int length = ReadInt("Input number of elements in array: ");

        Console.WriteLine($"Input {length} elements into array: ");
        int sum = 0;
        for (int i = 0; i < length; i++)
        {
            int num = ReadInt("");
            sum += num;
        }
        Console.WriteLine($"The sum is {sum}");
    }

    static void Task4()
    {
        Console.WriteLine("Task 4");

        int[] array1 = ReadArray();
        int[] array2 = new int[array1.Length];

        for (int i = 0; i < array1.Length; i++)
            array2[i] = array1[i];

        Console.WriteLine("Elements in the first array is:");
        PrintArray(array1);
        Console.WriteLine("Elements in the second array is:");
        PrintArray(array2);
    }

    static void Task5()
    {
        Console.WriteLine("Task 5");

        int[] array = ReadArray();
        Array.Sort(array);
        int duplicate = 0;
        for (int i = 1; i < array.Length; i++)
        {
            if (array[i] == array[i - 1]) duplicate++;
        }
        Console.WriteLine($"There are {duplicate} duplicates in the array");
    }

    static void Task6()
    {
        Console.WriteLine("Task 6");

        int[] array = ReadArray();
        Array.Sort(array);

        List<int> unique = new List<int>();
        // Initial element will always be unique
        if (array.Length == 0) return;
        unique.Add(array[0]);
        for (int i = 1; i < array.Length; i++)
        {
            if (array[i] == array[i - 1]) continue;
            unique.Add(array[i]);
            Console.WriteLine(i);
        }

        Console.WriteLine("Unique elements:");
        PrintArray(unique.ToArray());
        Console.WriteLine("");
    }

    static void Task7()
    {
        Console.WriteLine("Task 7");

        int[] array1 = ReadArray();
        int[] array2 = ReadArray();

        // Sort the two arrays individually to avoid sorting a larger array due to NlogN complexity
        Array.Sort(array1);
        Array.Sort(array2);

        int[] merged = new int[array1.Length + array2.Length];

        int left = 0, right = 0;

        for (int i = 0; i < merged.Length; i++)
        {
            if (left >= array1.Length || array1[left] > array2[right])
            {
                merged[i] = array2[right++];
            }
            else
            {
                merged[i] = array1[left++];
            }
        }
        Console.WriteLine("The merged array is:");
        PrintArray(merged);
    }

    static void Task8()
    {
        Console.WriteLine("Task 8");

        int[] array = ReadArray();
        Array.Sort(array);
        int occurences = 0;
        for (int i = 0; i < array.Length; i++)
        {
            occurences++;
            // Not end of list and the next one is eq
            if (i < array.Length - 1 && array[i] == array[i + 1]) continue;
            // Last one that is similar
            Console.WriteLine($"{array[i]} occurred {occurences} times");
            occurences = 0;
        }
    }

    static void Task9()
    {
        Console.WriteLine("Task 9");

        int[] array = ReadArray();
        int minimum = array[0];
        int maximum = array[0];
        foreach (int elem in array)
        {
            if (elem >= maximum) maximum = elem;
            if (elem <= minimum) minimum = elem;
        }

        Console.WriteLine($"The minimum is {minimum} and maximum is {maximum}");

    }

    static void Task10()
    {
        Console.WriteLine("Task 10");
        int[] array = ReadArray();
        PrintArray(array);
        int even_count = 0;
        int odd_count = 0;
        // Would dynamic arrays be better?
        // Count sized beforehand
        foreach (int elem in array)
        {
            if (elem % 2 == 0)
                even_count++;
            else
                odd_count++;
        }

        int[] even = new int[even_count];
        even_count = 0;
        int[] odd = new int[odd_count];
        odd_count = 0;

        foreach (int elem in array)
        {
            if (elem % 2 == 0)
                even[even_count++] = elem;
            else
                odd[odd_count++] = elem;
        }

        // Sort the smaller arrays
        Array.Sort(even);
        Array.Sort(odd);

        Console.WriteLine("The even elements are:");
        PrintArray(even);     
        Console.WriteLine("The odd elements are:");
        PrintArray(odd);
    }

    static void Task11()
    {
        Console.WriteLine("Task 11");
        int[] array = ReadArray();

        Array.Sort(array);

        Console.WriteLine("Array in ascending sorted order: ");
        PrintArray(array);

    }
    
    static void Task12()
    {
        Console.WriteLine("Task 12");
        int[] array = ReadArray();

        Array.Sort(array, new Comparison<int>((a,b) => b.CompareTo(a)));

        Console.WriteLine("Array in ascending sorted order: ");
        PrintArray(array);

    }

    static void Task13()
    {
        List<int> list = new List<int>(ReadArray());

        list.Sort();
        Console.WriteLine("Your list is: ");
        PrintArray(list.ToArray());
        int val = ReadInt("Input value to insert: ");

        for(int i = 0; i < list.Count; i++)
        {
            if(list[i] > val)
            {
                list.Insert(i, val);
                break;
            }
        }

        Console.WriteLine("Your new list is: ");
        PrintArray(list.ToArray());
    }

    // 14 is the same

    static void Main(string[] args)
    {
        //Task1();
        //Task2();
        //Task3();
        //Task4();
        //Task5();
        //Task6();
        //Task7();
        //Task8();
        //Task9();
        //Task10();
        //Task11();
        //Task12();
        Task13();
    }
}
