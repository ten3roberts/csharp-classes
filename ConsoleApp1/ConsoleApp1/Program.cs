﻿using System;
using System.Drawing;
using System.Security.Cryptography;

class Program
{
    static void Main(string[] args)
    {
        Random random = new Random();

        bool done = false;
        int i = 0;
        for (i = 0; !done; i++)
        {
            int val = random.Next() % 4096;
            switch (val)
            {
                case 0: Utils.PrintColored("Haha |Red|Loser"); break;
                case 69: Utils.PrintColored("Haha, very funny"); done = true; break;
                case 420: Utils.PrintColored("|Green|Hope it's legal"); break;
                default: Utils.PrintColored(val.ToString()); break;
            }
        }
        Utils.PrintColored($"It took {i} times to reach 69");

        Utils.PrintColored("This is a text string. |Red|This is now in red\n");
        Utils.PrintColored("|Blue|This is in blue\n");
        int x = 5;
        Utils.PrintColored($"x=|Black:Magenta|{x}\n");
        Utils.PrintColored("|:Green|Program Exited Successfully");
    }
}

