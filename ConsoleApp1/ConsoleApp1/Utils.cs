﻿using System;
using System.Collections.Generic;
using System.Text;

class Utils
{
    class Color
    {
        public ConsoleColor fg;
        public ConsoleColor bg;

        public Color(ConsoleColor fg, ConsoleColor bg)
        {
            this.fg = fg;
            this.bg = bg;
        }

        public Color()
        {
            fg = ConsoleColor.White;
            bg = ConsoleColor.Black;
        }

        public void SetConsoleColor()
        {
            Console.ForegroundColor = fg;
            Console.BackgroundColor = bg;
        }

        public static void Reset()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;
        }
    }
    // Prints a string to stdout
    // Every occurence of |color| will change following color
    // The color is specified by |foreground:background| or |foreground| with assumed black foreground
    // An empty color tag "||" resets the color
    // Accepted Colors are all those in System.ConsoleColor
    public static void PrintColored(string str)
    {
        // Reset color before printing
        Color.Reset();
        bool in_tag = false;
        Int32 tag_start = 0;
        for (int i = 0; i < str.Length; i++)
        {
            char c = str[i];

            if (c == '|')
            {
                // Finish coloring tag and change console color
                if (in_tag)
                {
                    string color_str = str.Substring(tag_start + 1, i - tag_start - 1);
                    Color color = ParseColor(color_str);
                    color.SetConsoleColor();
                    in_tag = false;
                }
                else
                {
                    in_tag = true;
                    tag_start = i;
                }
                continue;
            }

            if (!in_tag)
            {
                Console.Write(c);
            }
        }
        Console.Write('\n');
        Color.Reset();
    }

    // Attempts to parse a string "foreground:background"
    static Color ParseColor(string val)
    {
        // Return default color
        if (val == "")
        {
            return new Color();
        }

        Int32 sep = val.IndexOf(':');
        Color color = new Color();
        Int32 first_part = sep != -1 ? sep : val.Length;
        if (first_part != 0)
        {
            color.fg = Enum.Parse<ConsoleColor>(val.Substring(0, sep != -1 ? sep : val.Length));
        }
        if (sep != -1)
        {
            color.bg = Enum.Parse<ConsoleColor>(val.Substring(sep + 1));
        }
        return color;
    }
}