﻿using System;

namespace ConsoleApp2
{
    class Program
    {
        static bool ProcessYes(string val)
        {
            return val.ToLower().Contains("yes");
        }


        static void Main(string[] args)
        {
            string name = Console.ReadLine();
            Console.WriteLine($"Hello {name}");

            Console.WriteLine("Say, do you wan't to sum some numbers?");
            string answer = Console.ReadLine();
            if(!ProcessYes(answer))
            {
                Console.WriteLine("Well you see, I don't really care");
            }

            Console.Write("Give me an A: ");
            float a = Convert.ToInt32(Console.ReadLine());
            Console.Write("Give me a B: ");
            float b = Convert.ToInt32(Console.ReadLine());

            Console.Write($"I do believe the sum is {a + b}, but I'm not really sure\nDon't quote me on it!");
        }
    }
}
