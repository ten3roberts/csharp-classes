use std::io;
use std::io::Write;

fn prompt(string: &str) -> String {
    io::stdout().write(string.as_bytes()).unwrap();
    io::stdout().flush().unwrap();

    let mut buf = String::new();
    io::stdin().read_line(&mut buf).unwrap();
    buf.trim().to_owned()
}

fn prompt_yes(string: &str) -> bool {
    let input = prompt(string);

    match &input.trim().to_lowercase()[..] {
        "y" | "yes" | "yep" | "sure" | "ok" => true,
        _ => false,
    }
}

fn main() {
    let choice = prompt_yes("Enter something > ");

    match choice {
        true => println!("Great, let's play"),
        false => println!("I don't care"),
    };

    let input = prompt("Enter your move\n(r)ock, (p)apers, or (s)cissors > ");

    let attack = match Attack::new(&input) {
        Some(v) => v,
        None => panic!("Invalid attack"),
    };
    println!("You used {:?}", attack);

    let counter = attack.get_counter();

    println!("I use {:?}", counter);

    if attack == counter {
        println!("It's a draw");
    } else if attack == counter.get_counter() {
        println!("You win");
    } else if counter == attack.get_counter() {
        println!("I win, loser");
    }
}

#[derive(Debug, Eq, PartialEq)]
enum Attack {
    Rock,
    Paper,
    Scissors,
}

impl Attack {
    pub fn new(string: &str) -> Option<Self> {
        match &string.to_lowercase()[..] {
            "rock" | "r" => Some(Self::Rock),
            "paper" | "p" => Some(Self::Paper),
            "scissors" | "s" => Some(Self::Scissors),
            _ => None,
        }
    }

    pub fn get_counter(&self) -> Self {
        match self {
            Attack::Rock => Attack::Paper,
            Attack::Paper => Attack::Scissors,
            Attack::Scissors => Attack::Rock,
        }
    }
}
