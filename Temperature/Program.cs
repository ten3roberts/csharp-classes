﻿using System;
using System.Globalization;

namespace Temperature
{
    class Program
    {
        static int Main(string[] args)
        {
            // Ask the user
            Console.Write("Enter unit to convert to. (c)elsius or (f)ahrenheit > ");

            // Read the answer and convert it all to lowercase, to ignore uppercase and lowercase differences
            string unit = Console.ReadLine().ToLower();

            // Branch depending on answer
            // ---
            // What comes after in both branches is very repeated.
            // We could either prompt the used before branching, but that could be annoying of having to enter a number before we find out the unit was wrong
            // Unit validation can be done before hand and extracted into enum, but we haven't learnt that yet
            // We could also write a function that reads and converts to avoid this repetition, but we haven't learnt that too much as of yet
            // You could try and implement any of these improvements as is, as an execise
            // You could also do error handling on string conversion, as it now throws an exception
            if (unit == "c" || unit == "celsius")
            {
                Console.Write("Enter degrees in celsius > ");
                string input = Console.ReadLine();
                // Replace ',' to '.' to be locale agnostic
                input = input.Replace(',', '.');
                // Convert to floating point number
                float celsius = float.Parse(input, CultureInfo.InvariantCulture);

                // Do formual conversion
                float fahrenheit = celsius * 1.8f + 32f;
                // Print result
                Console.WriteLine($"That is {fahrenheit} degrees fahrenheit");
            }
            else if (unit == "f" || unit == "fahrenheit")
            {
                Console.Write("Enter degrees in fahrenheit > ");
                string input = Console.ReadLine();
                // Replace ',' to '.' to be locale agnostic
                input = input.Replace(',', '.');
                // Convert to floating point number
                float fahrenheit = float.Parse(input, CultureInfo.InvariantCulture);

                // Do formula conversion
                float celsius = (fahrenheit - 32f) / 1.8f;
                // Print result
                Console.WriteLine($"That is {celsius} degrees celsius");
            }
            // Edge case: Invalid unit
            else
            {
                Console.WriteLine("Please enter a valid unit!");
                return -1;
            }
            return 0;
        }
    }
}
