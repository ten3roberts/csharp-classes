﻿using System;
using System.Runtime.InteropServices.ComTypes;

class Program
{
    static void Task1(int lim)
    {
        for (int i = 1; i <= lim; i++)
        {
            Console.Write(i);
            if (i < lim) Console.Write(", ");
        }
        Console.Write("\n");
    }

    static void Task2(int lim)
    {
        Console.WriteLine($"The first {lim} natural numbers are: ");
        Task1(lim);

        int sum = 0;
        for (int i = 1; i <= lim; i++) sum += i;
        Console.WriteLine($"The sum is: {sum}");
    }

    static void Task4()
    {
        Console.WriteLine("Input 10 numbers separated by a newline");
        int sum = 0;
        for (int i = 0; i < 10; i++)
        {
            string line = Console.ReadLine();
            int num = Convert.ToInt32(line);
            sum += num;
        }
        Console.WriteLine($"The sum is {sum} and average {sum / 10}");
    }

    static void Task5()
    {
        Console.Write("Input number of terms > ");
        string line = Console.ReadLine();

        int lim = Convert.ToInt32(line);
        for (int i = 1; i <= lim; i++)
        {
            Console.WriteLine($"The cube of {i} is {i * i}");
        }
    }

    static void Task6OnSteroids()
    {
        Console.Write("Enter size of table > ");
        string line = Console.ReadLine();
        int max_size = Convert.ToInt32(line);
        int max_strwidth = (int)(Math.Log10(max_size * max_size)) + 3;
        for (int i = 1; i <= max_size; i++)
        {
            for (int j = 1; j <= max_size; j++)
            {
                Console.Write((i * j).ToString().PadLeft(max_strwidth, ' '));
            }
            Console.WriteLine("");
        }
    }

    static void Main(string[] args)
    {
        Task1(10);
        Task2(10);
        //Task4();
        Task5();
        Task6OnSteroids();
    }
}
