﻿using System;
using System.Linq;
using System.Reflection.PortableExecutable;

namespace Uppgift4
{
    class Program
    {
        static string Prompt(string prompt)
        {
            Console.Write(prompt);
            return Console.ReadLine();
        }

        static void Main(string[] args)
        {
            int i = 0;
            while (i < 100)
            {
                Console.WriteLine(i);
                i++;
            }

            i = 0;
            while (i < 100)
            {
                if (i % 2 == 0)
                    Console.WriteLine(i);
                i++;
            }

            string input = Prompt("Give me a starting value from 1 to 100 > ");
            int starting_index = Convert.ToInt32(input);
            Console.WriteLine("Here are your numbers: ");
            for (i = starting_index; i <= 101; i++)
            {
                Console.WriteLine("\t" + i);
            }
            PrintAlphabet();
            FlipACoin();
        }

        static void PrintAlphabet()
        {
            // Only latin characters
            for (char c = 'a'; c <= 'z'; c++)
            {
                Console.WriteLine(c);
            }
            Console.WriteLine("å");
            Console.WriteLine("ä");
            Console.WriteLine("ö");
        }

        enum Coin
        {
            Heads, Tails
        }

        static void FlipACoin()
        {
            Random random = new Random();

            string input = Prompt("How many times should I toss a coin? > ");
            int times = Convert.ToInt32(input);

            Console.WriteLine($"Tossing {times} times");
            int maxCoin = Enum.GetValues(typeof(Coin)).Cast<int>().Max();
            for (int i = 0; i < times; i++)
            {
                Coin coin = (Coin)random.Next(0, maxCoin+1);
                Console.WriteLine($"Got {coin}");
            }
        }
    }
}
